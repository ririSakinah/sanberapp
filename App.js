import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View } from "react-native";

import YoutubeUI from "./Tugas/tugas_12/App";
import Tugas13 from "./Tugas/tugas_13/Routes";
import Tugas14 from "./Tugas/tugas_14/App";
import Tugas15 from "./Tugas/tugas_15/index";
import TugasNavigation from "./Tugas/TugasNavigation/Routes";
import Quiz3 from "./Tugas/quiz_3/index";

export default function App() {
  return (
    //<YoutubeUI />
    //<Tugas13 />
    //<Tugas14 />
    //<Tugas15 />
    //<TugasNavigation />
    <Quiz3 />
    // <View style={styles.container}>
    //   <Text>Open up App.js to start working on your app!</Text>
    //   <StatusBar style="auto" />
    // </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
