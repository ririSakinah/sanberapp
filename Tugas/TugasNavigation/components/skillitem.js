import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
} from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class SkillItem extends Component {
  render() {
    let skill = this.props.skill;
    return (
      <View style={styles.container}>
        <View style={styles.posisi}>
          <Icon
            style={{
              alignItems: "center",
              justifyContent: "flex-start",
              marginTop: 10,
            }}
            name={skill.iconName}
            size={70}
            color="#003366"
          />
          <View style={styles.keterangan}>
            <Text style={styles.skillname}>{skill.skillName}</Text>
            <Text style={styles.categoryname}>{skill.categoryName}</Text>
            <Text style={styles.percent}>{skill.percentageProgress}</Text>
          </View>
          <Icon
            style={{
              alignItems: "center",
              justifyContent: "flex-end",
              marginTop: 10,
            }}
            name="chevron-right"
            size={70}
            color="#003366"
          />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    padding: 15,
    backgroundColor: "#B4E9FF",
    borderRadius: 10,
    paddingVertical: 3,
    paddingHorizontal: 10,
  },
  skillname: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#003366",
  },
  categoryname: {
    fontSize: 15,
    color: "#3EC6FF",
  },
  percent: {
    fontSize: 40,
    color: "white",
  },
  keterangan: {
    flexDirection: "column",
    justifyContent: "space-around",
  },
  posisi: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  tabItem: {
    alignItems: "center",
    justifyContent: "flex-end",
  },
});
