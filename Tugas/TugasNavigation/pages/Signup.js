import React, { Component } from "react";

import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  TouchableOpacity,
} from "react-native";

import Logo from "../components/Logo";
import Register from "../components/Register";

export default class Signup extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Logo />
        <Text style={styles.logoText}>Register</Text>
        <Register type="Register" />
        <View style={styles.signupTextCont}>
          <Text style={styles.signupText}>Sudah memiliki akun?</Text>
          <TouchableOpacity onPress={this.goBack}>
            <Text style={styles.signupButton}> Sign in</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#ffffff",
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },

  signupTextCont: {
    flexGrow: 1,
    alignItems: "flex-end",
    justifyContent: "center",
    paddingVertical: 16,
    flexDirection: "row",
  },

  signupText: {
    color: "#4e4e4e",
    fontSize: 16,
  },

  signupButton: {
    color: "#000000",
    fontSize: 16,
    fontWeight: "900",
  },

  logoText: {
    marginVertical: 15,
    fontSize: 27,
    color: "#003366",
    fontWeight: "900",
  },
});
