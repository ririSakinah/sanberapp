import React, { Component } from "react";

import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  TextInput,
} from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Logo from "../components/Logo";
import Form from "../components/Form";

export default class Login extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Logo />

        <Form />

        <View style={styles.signupTextCont}>
          <Text style={styles.signupText}>Belum memiliki akun?</Text>
          <TouchableOpacity onPress={() => navigation.navigate("Register")}>
            <Text style={styles.signupButton}> Register</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#ffffff",
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },

  signupTextCont: {
    flexGrow: 1,
    alignItems: "flex-end",
    justifyContent: "center",
    paddingVertical: 16,
    flexDirection: "row",
  },

  signupText: {
    color: "#4e4e4e",
    fontSize: 15,
  },

  signupButton: {
    color: "#000000",
    fontSize: 15,
    fontWeight: "500",
  },
  inputBox: {
    width: 300,
    backgroundColor: "#efefef",
    borderRadius: 18,
    paddingHorizontal: 20,
    paddingVertical: 5,
    fontSize: 16,
    color: "#000000",
    marginVertical: 10,
  },

  button: {
    width: 300,
    backgroundColor: "#030F3D",
    borderRadius: 18,
    marginVertical: 10,
    paddingVertical: 13,
  },

  buttonText: {
    fontSize: 16,
    fontWeight: "500",
    color: "#ffffff",
    textAlign: "center",
  },

  lupaPasswordCont: {
    color: "#000000",
    alignSelf: "center",
    flexDirection: "row",
  },
});
