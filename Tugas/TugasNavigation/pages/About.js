import React, { Component } from "react";

import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import AboutMe from "../components/AboutMe";
import Portofolio from "../components/portofolio";

export default class Signup extends Component {
  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <AboutMe />
          <Portofolio />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#ffffff",
    flex: 1,
    marginTop: 20,
  },
  title: {
    fontSize: 35,
    color: "#003366",
    fontWeight: "bold",
  },
  navItem: {
    color: "#e5e5e5",
  },
  name: {
    color: "#003366",
    fontSize: 20,
    fontWeight: "bold",
  },
  name2: {
    color: "#3EC6FF",
    fontSize: 17,
    fontWeight: "bold",
  },
  navBar: {
    backgroundColor: "#FFFFFF",
    elevation: 2,
    alignItems: "flex-start",
    justifyContent: "flex-start",
  },
  navItem: {
    marginLeft: 25,
  },
});
