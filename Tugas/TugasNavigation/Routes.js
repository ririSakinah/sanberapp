import React from "react";
import { StyleSheet } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";

import Login from "./pages/Login";
import Register from "./pages/Signup";
import About from "./pages/About";
import SkillScreen from "./pages/skillscreen";

const Stacknav = createStackNavigator();
const Drawernav = createDrawerNavigator();
const Tabnav = createBottomTabNavigator();

const Drawer = () => {
  <Drawer.Navigator>
    <Drawer.Screen name="About" component={About} />
    <Drawer.Screen name="SkillScreen" component={SkillScreen} />
  </Drawer.Navigator>;
};

export default class App extends React.Component {
  render() {
    return (
      //<Login />
      //<Register />
      //<About />
      <NavigationContainer>
        <Stacknav.Navigator initialRouteName="Login">
          <Stacknav.Screen name="Login" component={Login} />
          <Stacknav.Screen name="Drawer" component={Drawer} />
          <Stacknav.Screen name="Register" component={Register} />
        </Stacknav.Navigator>
      </NavigationContainer>
    );
  }
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
// });
