import React, { Component } from "react";

import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  TouchableOpacity,
} from "react-native";

import Logo from "../components/Logo";
import Form from "../components/Form";

export default class Login extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Logo />

        <Form type="Login" />

        <View style={styles.signupTextCont}>
          <Text style={styles.signupText}>Belum memiliki akun?</Text>
          <TouchableOpacity onPress={this.signup}>
            <Text style={styles.signupButton}> Register</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#ffffff",
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },

  signupTextCont: {
    flexGrow: 1,
    alignItems: "flex-end",
    justifyContent: "center",
    paddingVertical: 16,
    flexDirection: "row",
  },

  signupText: {
    color: "#4e4e4e",
    fontSize: 15,
  },

  signupButton: {
    color: "#000000",
    fontSize: 15,
    fontWeight: "500",
  },
});
