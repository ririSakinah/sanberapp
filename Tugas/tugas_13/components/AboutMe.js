import React, { Component } from "react";

import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";

export default class Signup extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Tentang Saya</Text>
        <Icon style={styles.navItem} name="account-circle" size={200} />
        <Text style={styles.name}>Riri Sakinah</Text>
        <Text style={styles.name2}>React Native</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#ffffff",
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    fontSize: 35,
    color: "#003366",
    fontWeight: "bold",
  },
  navItem: {
    color: "#e5e5e5",
  },
  name: {
    color: "#003366",
    fontSize: 20,
    fontWeight: "bold",
  },
  name2: {
    color: "#3EC6FF",
    fontSize: 17,
    fontWeight: "bold",
  },
});
