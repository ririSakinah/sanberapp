import React, { Component } from "react";

import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from "react-native";

export default class Register extends Component {
  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <TextInput
            style={styles.inputBox}
            underlineColorAndroid="rgba(0,0,0,0)"
            placeholder="Username"
            placeholderTextColor="#999999"
            selectionColor="#000000"
            keyboardType="email-address"
          />
          <TextInput
            style={styles.inputBox}
            underlineColorAndroid="rgba(0,0,0,0)"
            placeholder="Konfirmasi Email"
            placeholderTextColor="#999999"
            selectionColor="#fff"
            keyboardType="email-address"
          />
          <TextInput
            style={styles.inputBox}
            underlineColorAndroid="rgba(0,0,0,0)"
            placeholder="Password"
            secureTextEntry={true}
            placeholderTextColor="#999999"
            ref={(input) => (this.password = input)}
          />
          <TextInput
            style={styles.inputBox}
            underlineColorAndroid="rgba(0,0,0,0)"
            placeholder="Konfirmasi Password"
            secureTextEntry={true}
            placeholderTextColor="#999999"
            ref={(input) => (this.password = input)}
          />
          <TouchableOpacity style={styles.button}>
            <Text style={styles.buttonText}>{this.props.type}</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: "center",
    alignItems: "center",
  },

  inputBox: {
    width: 300,
    backgroundColor: "#efefef",
    borderRadius: 18,
    paddingHorizontal: 20,
    paddingVertical: 5,
    fontSize: 16,
    color: "#000000",
    marginVertical: 10,
  },

  button: {
    width: 300,
    backgroundColor: "#030F3D",
    borderRadius: 25,
    marginVertical: 10,
    paddingVertical: 13,
  },

  buttonText: {
    fontSize: 16,
    fontWeight: "500",
    color: "#ffffff",
    textAlign: "center",
  },
});
