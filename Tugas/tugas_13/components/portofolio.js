import React, { Component } from "react";

import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import { SocialIcon } from "react-native-elements";

export default class Signup extends Component {
  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.garis}>
            <Text style={styles.judul}>Portofolio</Text>
            <View style={styles.icon1}>
              <View style={styles.tabItem}>
                <SocialIcon type="gitlab" />
                <Text style={styles.tabTitle}>@ririSakinah</Text>
              </View>
              <View style={styles.tabItem}>
                <SocialIcon type="github" />
                <Text style={styles.tabTitle}>@ririSakinah</Text>
              </View>
            </View>
          </View>

          <View style={styles.garis}>
            <Text style={styles.judul}>Hubungi Saya</Text>
            <View style={styles.sosmed}>
              <View style={styles.descContainer}>
                <SocialIcon type="twitter" />
                <Text style={styles.tabtitle2}>@yellowsimisimi</Text>
              </View>
              <View style={styles.descContainer}>
                <SocialIcon type="instagram" />
                <Text style={styles.tabtitle2}>Risa Muhammad</Text>
              </View>
              <View style={styles.descContainer}>
                {/* <Icon name="home" size={40} color="#999999" /> */}
                <SocialIcon type="facebook" color="#3EC6FF" />
                <Text style={styles.tabtitle2}>Risa Muhammad</Text>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#ffffff",
    flex: 1,
  },
  judul: {
    color: "#003366",
    fontSize: 16,
    margin: 20,
    borderBottomColor: "#003366",
    borderBottomWidth: 1,
  },
  icon1: {
    backgroundColor: "white",

    borderColor: "#E5E5E5",
    flexDirection: "row",
    justifyContent: "space-around",
  },
  tabItem: {
    alignItems: "center",
    justifyContent: "center",
  },
  descContainer: {
    flexDirection: "row",
    paddingTop: 10,
    justifyContent: "center",
  },
  sosmed: {
    backgroundColor: "white",
    borderColor: "#E5E5E5",
    flexDirection: "column",
    justifyContent: "space-around",
  },
  tabTitle: {
    fontSize: 16,
    color: "#003366",
    paddingTop: 4,
  },
  tabtitle2: {
    fontSize: 16,
    color: "#003366",
    paddingTop: 15,
    marginLeft: 20,
  },
});
