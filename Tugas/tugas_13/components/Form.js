import React, { Component } from "react";

import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from "react-native";
//import { Actions } from "react-native-router-flux";

export default class Logo extends Component {
  // resetpassword() {
  //   Actions.resetpassword();
  // }

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <TextInput
            style={styles.inputBox}
            underlineColorAndroid="rgba(0,0,0,0)"
            placeholder="Email atau No. HP"
            placeholderTextColor="#999999"
            selectionColor="#000000"
            keyboardType="email-address"
            onSubmitEditing={() => this.password.focus()}
          />

          <TextInput
            style={styles.inputBox}
            underlineColorAndroid="rgba(0,0,0,0)"
            placeholder="Password"
            secureTextEntry={true}
            placeholderTextColor="#999999"
            ref={(input) => (this.password = input)}
          />

          <TouchableOpacity style={styles.button}>
            <Text style={styles.buttonText}>{this.props.type}</Text>
          </TouchableOpacity>

          <View style={styles.lupaPasswordCont}>
            <Text style={styles.signupText}>Lupa Kata Sandi?</Text>
            <TouchableOpacity onPress={this.resetpassword}>
              <Text style={styles.signupButton}> Klik disini</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: "center",
    alignItems: "center",
  },

  inputBox: {
    width: 300,
    backgroundColor: "#efefef",
    borderRadius: 18,
    paddingHorizontal: 20,
    paddingVertical: 5,
    fontSize: 16,
    color: "#000000",
    marginVertical: 10,
  },

  button: {
    width: 300,
    backgroundColor: "#030F3D",
    borderRadius: 18,
    marginVertical: 10,
    paddingVertical: 13,
  },

  buttonText: {
    fontSize: 16,
    fontWeight: "500",
    color: "#ffffff",
    textAlign: "center",
  },

  lupaPasswordCont: {
    color: "#000000",
    alignSelf: "center",
    flexDirection: "row",
  },
  signupText: {
    color: "#4e4e4e",
    fontSize: 14,
  },
  signupButton: {
    color: "#000000",
    fontSize: 14,
    fontWeight: "500",
  },
});
