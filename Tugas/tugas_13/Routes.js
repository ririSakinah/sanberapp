import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View } from "react-native";

import Login from "./pages/Login";
import Register from "./pages/Signup";
import About from "./pages/About";

export default function App() {
  return (
    //<Login />
    <Register />
    //<About />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
