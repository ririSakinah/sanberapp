import { Component } from "react";
import React from "react";
import Main from "./components/Main";
import Data from "./skillData.json";

import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  Image,
} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import Skillitem from "./components/skillitem";

export default class Skill extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View>
          <View style={styles.navBar}>
            <Image
              source={require("../tugas_13/images/logoapp.png")}
              style={{ width: 130, height: 40 }}
            />
          </View>

          <View style={styles.sosmed}>
            <View style={styles.descContainer}>
              <Icon
                style={{ marginLeft: 15 }}
                name="account-circle"
                size={40}
                color="#3EC6FF"
              />
              <Text style={styles.tabtitle2}>Hai, Yellow Simi</Text>
            </View>
          </View>
        </View>
        <Text style={styles.judul}>SKILL</Text>
        <View style={{ marginLeft: 5, marginRight: 5 }}>
          <View style={styles.icon1}>
            <View style={styles.tabItem}>
              <Text style={styles.tabTitle}>Librari/Framework</Text>
            </View>
            <View style={styles.tabItem}>
              <Text style={styles.tabTitle}>Bahasa Pemrograman</Text>
            </View>
            <View style={styles.tabItem}>
              <Text style={styles.tabTitle}>Tegnologi</Text>
            </View>
          </View>
        </View>

        <View style={styles.body}>
          <FlatList
            data={Data.items}
            renderItem={(skill) => <Skillitem skill={skill.item} />}
            keyExtractor={(item) => item.id}
            ItemSeparatorComponent={() => (
              <View style={{ height: 10, backgroundColor: "#ffffff" }} />
            )}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  navBar: {
    height: 60,
    backgroundColor: "#FFFFFF",
    paddingHorizontal: 15,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  sosmed: {
    backgroundColor: "white",
    borderColor: "#E5E5E5",
    flexDirection: "column",
    justifyContent: "space-around",
  },
  descContainer: {
    flexDirection: "row",
    paddingTop: 10,
    justifyContent: "flex-start",
  },
  tabtitle: {
    fontSize: 10,
    color: "#003366",
  },
  judul: {
    color: "#003366",
    fontSize: 25,
    margin: 15,
    borderBottomColor: "#B4E9FF",
    borderBottomWidth: 3,
    fontWeight: "bold",
  },
  icon1: {
    backgroundColor: "white",

    borderColor: "#E5E5E5",
    flexDirection: "row",
    justifyContent: "space-around",
  },
  tabItem: {
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: 5,
    paddingVertical: 3,
    backgroundColor: "#3EC6FF",
    borderRadius: 10,
  },
  tabtitle2: {
    fontSize: 16,
    color: "#003366",
    paddingTop: 10,
    marginLeft: 15,
  },
  body: {
    flex: 1,
    margin: 10,
  },
});
